<div id="content">
            <div class="inner">
                <div class="row">
                <div class="col-md-12">
                    <h3><span class="fa fa-users"></span> MASTER LEVEL</h3>
                </div>
                <div class="col-md-12">
                    <hr>
                    <div class="text-right"><b><i><span class="fa fa-home"></span> Home / <span class="fa fa-list"></span> Master Data / <span class="fa fa-list"> </span> Level</i></b></div>
                    <hr>
                </div>
            </div>
                <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary" style="border-color:#f8f8f8;">
                        <div class="panel-heading">
                            <span class="fa fa-list"></span> Master Level <a href="index.php?hal=level/add" class="btn btn-sm btn-success dim_about"><span class="fa fa-plus"></span> Tambah Data</a>
                        </div>
                        <div class="panel-body dim_about">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="2%">NO</th>
                                            <th width="10%">NAMA LEVEL</th>
                                            <th width="10%">AKSI</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $no = 1;
                                        $query = mysql_query("SELECT * from ref_level");
                                        while ($row = mysql_fetch_array($query)) {
                                     ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $row['level_name']; ?></td>
                                            <td>
                                                <a href="index.php?hal=level/edit&id=<?php echo $row['level_id']; ?>" class="btn btn-warning btn-xs"><span class="fa fa-edit"></span> Ubah</a>
                                                <a href="index.php?hal=level/list&hapus=<?php echo $row['level_id']; ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Hapus</a>
                                                <a href="index.php?hal=level/fasilitas_level&id=<?php echo $row['level_id']; ?>" class="btn btn-info btn-xs"><span class="fa fa-lock"></span>Ubah Fasilitas</a>   
                                            </td>
                                            
                                        </tr>
                                       <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
