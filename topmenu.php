<div class="navbar-wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand dim_about" href="index.php">LBP PAELANTOLOGI - UGM</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="page-scroll" href="index.php#page-top" style="color:black; ">Home</a></li>
                        <li><a class="page-scroll" href="index.php#features" style="color:black; ">Prosedur</a></li>
                        <li><a class="page-scroll" href="index.php#team" style="color:black; ">Layanan</a></li>
                         <?php 
                            if (isset($_SESSION['member_name'])) {
                                echo "<li><a class='page-scroll' style='color:black; ' href='index.php?hal=members/list'>USERPROFIL</a></li>";
                                echo "<li><a class='page-scroll' style='color:black; ' href='index.php?logout=1'>KELUAR</a></li>";
                            }else{
                                echo " <li><a class='page-scroll' style='color:black; ' href='index.php#daftar'>DAFTAR</a></li>";
                            }
                         ?>
                        <li><a class="page-scroll" href="index.php#contact" style="color:black; ">Kontak</a></li>
                    </ul>
                </div>
            </div>
        </nav>
</div>