-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2017 at 03:34 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `penyewaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `ref_category`
--

CREATE TABLE IF NOT EXISTS `ref_category` (
`category_id` tinyint(4) NOT NULL,
  `category_name` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_category`
--

INSERT INTO `ref_category` (`category_id`, `category_name`) VALUES
(1, 'Mahasiswa Kedokteran  S1 UGM'),
(2, 'Mahasiswa'),
(3, 'Peneliti'),
(4, 'Umum');

-- --------------------------------------------------------

--
-- Table structure for table `ref_instrument`
--

CREATE TABLE IF NOT EXISTS `ref_instrument` (
`instrument_id` tinyint(5) NOT NULL,
  `instrument_name` varchar(50) NOT NULL,
  `instrument_brand` varchar(20) NOT NULL,
  `instrument_quantity` int(5) NOT NULL,
  `instrument_fee` float NOT NULL,
  `instrument_description` text NOT NULL,
  `instrument_length` varchar(15) NOT NULL,
  `instrument_weight` varchar(10) NOT NULL,
  `instrument_picture` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_instrument`
--

INSERT INTO `ref_instrument` (`instrument_id`, `instrument_name`, `instrument_brand`, `instrument_quantity`, `instrument_fee`, `instrument_description`, `instrument_length`, `instrument_weight`, `instrument_picture`) VALUES
(15, 'Anthropometer', 'Holtain Limited, Har', 2, 50000, '', '-', '-', 'IMG_1669.JPG'),
(16, 'Spreadhing Caliper / Caliper Rentang', 'GPM, Swiss', 1, 50000, '', '60 dan 30 ', '-', 'IMG_1679.JPG'),
(17, 'Spreading Caliper Lengkung', 'GPM, Swiss', 1, 50000, 'Ketelitian 1,5 ', '50 cm', '-', 'IMG_1686.JPG'),
(18, 'Spreading Caliper Lengkung', 'GPM, Swiss', 1, 50000, 'Ketelitian 1 ', '50 cm', '-', 'IMG_1697.JPG'),
(19, 'Dynamo Meter Set', 'TTM, Tokyo', 1, 50000, '2 tangan', '-', '100 kg', 'IMG_1710.JPG'),
(20, 'Dynamo Meter Set', 'TTM, Tokyo', 1, 50000, '1 tangan', '-', '50 kg', 'IMG_1712.JPG'),
(21, 'Dynamo Meter Set', 'TTM, Tokyo', 1, 50000, '', '-', '-', 'IMG_1713.JPG'),
(22, 'A.OTT Compensating Polar Plammeter', 'Kempten Bayern', 1, 50000, '', '21 mm', '-', 'IMG_1717.JPG'),
(23, 'Lange Skinfold Caliper', 'Cambridge Scientific', 2, 50000, '', '60 mm', '-', 'IMG_1720.JPG'),
(24, 'Skinfold Caliper ', 'Holtain', 3, 50000, '', '0,2 mm', '-', 'IMG_1728.JPG'),
(25, 'Sliding Caliper', 'Holtain', 2, 50000, '', '14 cm', '-', 'IMG_1729.JPG'),
(26, 'Sliding Caliper / Vernier Caliper', 'Mitutoyo, Japan', 1, 50000, '', '300 mm', '-', 'IMG_1734.JPG'),
(27, 'Sliding Caliper', 'GPM, Swiss', 1, 50000, '', '22 mm', '-', 'IMG_1736.JPG'),
(28, 'Sliding Caliper', 'GPM, Swiss', 1, 50000, '', '2 mm', '-', 'IMG_1742.JPG'),
(29, 'Sliding Caliper', 'GPM, Swiss', 1, 50000, '', '180 cm', '-', 'IMG_1745.JPG'),
(30, 'Pita Meter', 'Lutkin', 4, 50000, '', '2 m', '-', 'IMG_1747.JPG'),
(31, 'Orchiometer', 'Holtain', 2, 50000, '', '-', '-', 'IMG_1748.JPG'),
(32, 'Biotrop', 'Twinlite, Eschma Eng', 1, 50000, '  tesss ss', '-', '-', 'mysql-php-logos.png');

-- --------------------------------------------------------

--
-- Table structure for table `ref_level`
--

CREATE TABLE IF NOT EXISTS `ref_level` (
`level_id` tinyint(4) NOT NULL,
  `level_name` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_level`
--

INSERT INTO `ref_level` (`level_id`, `level_name`) VALUES
(1, 'admin'),
(2, 'koordinator penelitian'),
(3, 'kepala laboratorium'),
(4, 'member'),
(5, 'kasiran');

-- --------------------------------------------------------

--
-- Table structure for table `ref_level_menu`
--

CREATE TABLE IF NOT EXISTS `ref_level_menu` (
  `level_id` tinyint(4) NOT NULL,
  `menu_id` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_level_menu`
--

INSERT INTO `ref_level_menu` (`level_id`, `menu_id`) VALUES
(5, 5),
(5, 6),
(5, 9),
(2, 1),
(2, 4),
(3, 4),
(0, 14),
(0, 17),
(4, 14),
(4, 17),
(4, 26),
(4, 28),
(1, 2),
(1, 5),
(1, 6),
(1, 9),
(1, 10),
(1, 14),
(1, 17),
(1, 26),
(1, 29),
(1, 30),
(1, 31);

-- --------------------------------------------------------

--
-- Table structure for table `ref_menu`
--

CREATE TABLE IF NOT EXISTS `ref_menu` (
`menu_id` tinyint(4) NOT NULL,
  `menu_name` varchar(30) NOT NULL,
  `menu_url` varchar(50) NOT NULL,
  `menu_parent` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_menu`
--

INSERT INTO `ref_menu` (`menu_id`, `menu_name`, `menu_url`, `menu_parent`) VALUES
(1, 'Master Data', '', 0),
(2, 'Operator', 'operator/list', 1),
(5, 'Menu', 'menu/list', 1),
(6, 'Level', 'level/list', 1),
(9, 'Member', 'member/list', 1),
(10, 'Alat', 'alat/list', 1),
(13, 'Transaksi', '', 0),
(14, 'Pembayaran', 'pembayaran/list', 18),
(18, 'Konfirmasi', '', 0),
(19, 'Laporan', '', 0),
(26, 'Pengajuan Pinjaman', 'peminjaman/pengajuan/list', 18),
(28, 'Peminjaman', 'loan-list.php', 13),
(29, 'Pengembalian', '-', 13),
(30, 'Saldo', '-', 13),
(31, 'Denda', '-', 13);

-- --------------------------------------------------------

--
-- Table structure for table `ref_operator`
--

CREATE TABLE IF NOT EXISTS `ref_operator` (
`operator_id` tinyint(8) NOT NULL,
  `operator_name` varchar(25) DEFAULT NULL,
  `operator_gender` enum('Laki-laki','Perempuan') NOT NULL,
  `operator_username` varchar(8) DEFAULT NULL,
  `operator_password` char(32) DEFAULT NULL,
  `operator_login` enum('N','Y') DEFAULT NULL,
  `operator_hint_question` varchar(40) DEFAULT NULL,
  `operator_answer_question` varchar(30) DEFAULT NULL,
  `level_id_fk` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_operator`
--

INSERT INTO `ref_operator` (`operator_id`, `operator_name`, `operator_gender`, `operator_username`, `operator_password`, `operator_login`, `operator_hint_question`, `operator_answer_question`, `level_id_fk`) VALUES
(6, 'yudi', 'Laki-laki', 'yuyu', 'b6c43562aa8b71cc1797f8d8b9c2526a', 'N', 'Siapa nama ayah kandung anda?', 'joko', 2),
(7, 'kiki', 'Laki-laki', 'kikiki', 'a0bace38478f0e4acdf2a09135496cab', 'N', 'Siapa nama penyanyi kesukaan anda?', 'exo', 1),
(13, 'amanda aliasari', 'Perempuan', 'amanda', '21232f297a57a5a743894a0e4a801fc3', 'N', 'Apa warna favorit anda?', 'hijau', 1),
(16, 'hardi', 'Laki-laki', 'diwo', '550e1bafe077ff0b0b67f4e32f29d751', 'N', 'Apa warna favorit anda?', 'hijau', 2),
(18, 'lala ', 'Perempuan', 'lili', 'fc89ae5e64b38961a4ec8c5b90bafa69', 'N', 'Apa makanan favorit anda?', 'cumi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member`
--

CREATE TABLE IF NOT EXISTS `tbl_member` (
`member_id` int(4) NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `member_birth_date` date NOT NULL,
  `member_gender` enum('Laki-laki','Perempuan') NOT NULL,
  `member_phone_number` varchar(13) NOT NULL,
  `member_address` text NOT NULL,
  `member_username` varchar(15) NOT NULL,
  `member_password` char(32) NOT NULL,
  `member_hint_question` mediumtext NOT NULL,
  `member_answer_question` mediumtext NOT NULL,
  `member_institution` varchar(50) NOT NULL,
  `member_faculty` varchar(50) NOT NULL,
  `member_email` varchar(30) NOT NULL,
  `member_idcard_photo` varchar(25) NOT NULL,
  `member_photo` varchar(25) NOT NULL,
  `member_status` varchar(15) NOT NULL,
  `member_login` enum('Y','N') NOT NULL,
  `member_register_date` date NOT NULL,
  `member_confirm_date` date NOT NULL,
  `category_id_fk` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_member`
--

INSERT INTO `tbl_member` (`member_id`, `member_name`, `member_birth_date`, `member_gender`, `member_phone_number`, `member_address`, `member_username`, `member_password`, `member_hint_question`, `member_answer_question`, `member_institution`, `member_faculty`, `member_email`, `member_idcard_photo`, `member_photo`, `member_status`, `member_login`, `member_register_date`, `member_confirm_date`, `category_id_fk`) VALUES
(4, 'ahmad bastiar', '2017-06-01', 'Laki-laki', '085226213902', 'jepara', 'bastiar', 'aafc9468b4ba6c5f602618e0195a7588', 'Dimana Kota Anda Dilahirkan ?', 'Bukit Berbunga, riau', 'UGM', 'S1 KEDOKTERAN', 'ahmad.bastiar@gin.co.id', '10125136', '', 'Actived', 'N', '2017-01-27', '0000-00-00', 2),
(5, 'amanda aliyasari', '0000-00-00', 'Laki-laki', '085226213902', 'purworejo', 'amandaalyasari', '5e111b8915a1353c9b9e0353e3dde5dc', 'Dimana Kota Anda Dilahirkan ?', 'Purworejo', 'UGM', 'S1 KEDOKTERAN UGM', 'amandaalyasari@gmail.com', '10125136', '', 'Actived', 'N', '2017-01-28', '0000-00-00', 2),
(6, 'amandaalya', '0000-00-00', 'Laki-laki', '085226213903', 'jogja', 'amandasari', 'e3b52fb9a10de6661d54771f3f719584', 'Dimana Kota Anda Dilahirkan ?', 'Purworejo', 'UGM', 'S1 KEDOKTERAN UGM', 'amandasari@gmail.com', '10125136', '', 'Actived', 'N', '2017-01-28', '0000-00-00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_saldo`
--

CREATE TABLE IF NOT EXISTS `tbl_saldo` (
`saldo_id` int(11) NOT NULL,
  `loan_app_id_fk` char(5) NOT NULL,
  `saldo_nominal` int(11) NOT NULL,
  `member_id_fk` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_saldo`
--

INSERT INTO `tbl_saldo` (`saldo_id`, `loan_app_id_fk`, `saldo_nominal`, `member_id_fk`) VALUES
(5, '00001', 40000, 21),
(6, '00002', 50000, 23),
(7, '00003', 0, 24),
(8, '00007', 0, 6),
(9, '00008', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `trx_lease`
--

CREATE TABLE IF NOT EXISTS `trx_lease` (
`lease_id` tinyint(4) NOT NULL,
  `loan_id_fk` tinyint(4) NOT NULL,
  `member_id_fk` tinyint(4) NOT NULL,
  `member_name` varchar(30) NOT NULL,
  `lease_date` date NOT NULL,
  `return_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_lease_detail`
--

CREATE TABLE IF NOT EXISTS `trx_lease_detail` (
`lease_detail_id` tinyint(4) NOT NULL,
  `lease_id_fk` tinyint(4) NOT NULL,
  `instrument_id_fk` tinyint(4) NOT NULL,
  `lease_quantity` int(5) NOT NULL,
  `lease_amount` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_loan_application`
--

CREATE TABLE IF NOT EXISTS `trx_loan_application` (
  `loan_app_id` char(5) NOT NULL,
  `loan_invoice` varchar(20) NOT NULL,
  `loan_date_input` datetime NOT NULL,
  `loan_date_start` date NOT NULL,
  `loan_date_return` date NOT NULL,
  `loan_file` varchar(100) NOT NULL,
  `loan_total_item` int(11) NOT NULL,
  `loan_total_fee` int(11) NOT NULL,
  `loan_status` char(20) NOT NULL,
  `member_id_fk` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_loan_application`
--

INSERT INTO `trx_loan_application` (`loan_app_id`, `loan_invoice`, `loan_date_input`, `loan_date_start`, `loan_date_return`, `loan_file`, `loan_total_item`, `loan_total_fee`, `loan_status`, `member_id_fk`) VALUES
('00001', '00001/INV/17', '2017-01-25 13:20:28', '2017-01-27', '2017-01-27', 'modul_mysql.pdf', 4, 300000, 'MEMBAYAR TAGIHAN', 21),
('00002', '00002/INV/17', '2017-01-25 17:13:32', '2017-01-27', '2017-01-28', 'modul_mysql.pdf', 4, 200000, 'MEMBAYAR TAGIHAN', 23),
('00003', '00003/INV/17', '2017-01-26 11:57:47', '2017-01-28', '2017-02-04', 'Pengumuman_Wisuda_Periode_61.pdf', 2, 100000, 'MEMBAYAR TAGIHAN', 24),
('00004', '00004/INV/17', '2017-01-26 14:08:04', '2017-01-26', '2017-01-27', '0125_Ahmad Bastiar_4.pdf', 4, 200000, 'DITERIMA', 21),
('00005', '00005/INV/17', '2017-01-27 12:12:08', '0000-00-00', '0000-00-00', 'TUTORIAL PHP-MYSQL-IMPLEMENTASI.pdf', 2, 100000, 'DITERIMA', 25),
('00006', '00006/INV/17', '2017-01-27 19:03:00', '2017-01-27', '2017-01-28', 'modul_mysql.pdf', 2, 100000, 'ACC', 4),
('00007', '00007/INV/17', '2017-01-28 12:37:19', '2017-01-28', '2017-01-31', 'modul_mysql.pdf', 2, 100000, 'MEMBAYAR TAGIHAN', 6),
('00008', '00008/INV/17', '2017-01-28 17:52:10', '2017-01-28', '2017-01-31', 'modul_mysql.pdf', 3, 150000, 'MEMBAYAR TAGIHAN', 6);

-- --------------------------------------------------------

--
-- Table structure for table `trx_loan_application_detail`
--

CREATE TABLE IF NOT EXISTS `trx_loan_application_detail` (
`loan_app_detail_id` int(11) NOT NULL,
  `loan_app_id_fk` char(5) NOT NULL,
  `instrument_id_fk` int(11) NOT NULL,
  `loan_amount` int(11) NOT NULL,
  `loan_subtotal` int(11) NOT NULL,
  `loan_status_detail` char(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_loan_application_detail`
--

INSERT INTO `trx_loan_application_detail` (`loan_app_detail_id`, `loan_app_id_fk`, `instrument_id_fk`, `loan_amount`, `loan_subtotal`, `loan_status_detail`) VALUES
(14, '00001', 28, 1, 100000, 'MENUNGGU'),
(15, '00001', 29, 1, 100000, 'ACC'),
(16, '00001', 30, 2, 100000, 'ACC'),
(17, '00002', 28, 1, 50000, 'MENUNGGU'),
(18, '00002', 29, 1, 50000, 'ACC'),
(19, '00002', 30, 1, 50000, 'ACC'),
(20, '00002', 31, 1, 50000, 'ACC'),
(21, '00003', 28, 1, 50000, 'MENUNGGU'),
(22, '00003', 30, 1, 50000, 'ACC'),
(23, '00004', 16, 1, 50000, 'ACC'),
(24, '00004', 17, 1, 50000, 'ACC'),
(25, '00004', 23, 1, 50000, 'ACC'),
(26, '00004', 32, 1, 50000, 'ACC'),
(27, '00005', 28, 1, 50000, 'MENUNGGU'),
(28, '00005', 29, 1, 50000, 'ACC'),
(29, '00006', 30, 1, 50000, 'ACC'),
(30, '00006', 31, 1, 50000, 'ACC'),
(31, '00007', 29, 1, 50000, 'ACC'),
(32, '00007', 30, 1, 50000, 'ACC'),
(33, '00008', 29, 1, 50000, 'ACC'),
(34, '00008', 30, 1, 50000, 'ACC'),
(35, '00008', 31, 1, 50000, 'ACC');

-- --------------------------------------------------------

--
-- Table structure for table `trx_loan_cart`
--

CREATE TABLE IF NOT EXISTS `trx_loan_cart` (
`cart_id` tinyint(4) NOT NULL,
  `loan_app_id_fk` int(6) DEFAULT NULL,
  `instrument_id_fk` tinyint(4) NOT NULL,
  `cart_name` varchar(25) NOT NULL,
  `cart_quantity` int(4) NOT NULL,
  `cart_fee` float NOT NULL,
  `cart_sub_total` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_loan_cart`
--

INSERT INTO `trx_loan_cart` (`cart_id`, `loan_app_id_fk`, `instrument_id_fk`, `cart_name`, `cart_quantity`, `cart_fee`, `cart_sub_total`) VALUES
(1, NULL, 9, '', 2, 20000, 40000);

-- --------------------------------------------------------

--
-- Table structure for table `trx_loan_temp`
--

CREATE TABLE IF NOT EXISTS `trx_loan_temp` (
  `instrument_id_fk` int(5) NOT NULL,
  `member_id_fk` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_loan_temp`
--

INSERT INTO `trx_loan_temp` (`instrument_id_fk`, `member_id_fk`) VALUES
(31, 21),
(30, 21),
(19, 6),
(18, 6);

-- --------------------------------------------------------

--
-- Table structure for table `trx_payment`
--

CREATE TABLE IF NOT EXISTS `trx_payment` (
`payment_id` int(4) NOT NULL,
  `payment_amount_transfer` int(10) NOT NULL,
  `payment_amount_saldo` int(10) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_photo` varchar(25) NOT NULL,
  `payment_info` mediumtext NOT NULL,
  `payment_confirm_date` date NOT NULL,
  `payment_verification_date` date NOT NULL,
  `payment_notif` mediumtext NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `payment_saldo` int(10) NOT NULL,
  `loan_app_id_fk` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_payment_confirm`
--

CREATE TABLE IF NOT EXISTS `trx_payment_confirm` (
`payment_confirm_id` tinyint(4) NOT NULL,
  `lease_id_fk` tinyint(4) NOT NULL,
  `bank_name` varchar(20) NOT NULL,
  `account_number` varchar(15) NOT NULL,
  `on_behalf` varchar(30) NOT NULL,
  `payment_confirm_date` date NOT NULL,
  `payment_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_payment_temp`
--

CREATE TABLE IF NOT EXISTS `trx_payment_temp` (
`payment_temp_id` int(4) NOT NULL,
  `bankname` char(30) NOT NULL,
  `payment_bill` int(8) NOT NULL,
  `payment_temp_amount_transfer` int(10) NOT NULL,
  `payment_temp_amount_saldo` int(10) NOT NULL,
  `payment_temp_date` datetime NOT NULL,
  `payment_temp_confirm_date` datetime NOT NULL,
  `payment_temp_photo` varchar(25) NOT NULL,
  `payment_temp_info` mediumtext NOT NULL,
  `loan_app_id_fk` char(5) NOT NULL,
  `member_id_fk` tinyint(4) NOT NULL,
  `payment_notif` varchar(50) NOT NULL,
  `payment_status` char(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_payment_temp`
--

INSERT INTO `trx_payment_temp` (`payment_temp_id`, `bankname`, `payment_bill`, `payment_temp_amount_transfer`, `payment_temp_amount_saldo`, `payment_temp_date`, `payment_temp_confirm_date`, `payment_temp_photo`, `payment_temp_info`, `loan_app_id_fk`, `member_id_fk`, `payment_notif`, `payment_status`) VALUES
(14, 'BANK MANDIRI', 300000, 340000, 40000, '2017-01-25 00:00:00', '2017-01-27 00:00:00', 'dl.png', '-', '00001', 21, '', ''),
(15, 'BANK MANDIRI', 200000, 250000, 50000, '2017-01-25 00:00:00', '2017-01-27 00:00:00', 'modul_mysql.pdf', 'pembayaran tanpa saldo', '00002', 23, '', ''),
(16, 'BANK MANDIRI', 100000, 100000, 0, '2017-01-26 00:00:00', '2017-01-26 00:00:00', 'SETTING SKPD BANK.png', '-pembayaran peminjaman alat', '00003', 24, '', ''),
(17, 'BANK MANDIRI', 100000, 100000, 0, '2017-01-28 00:00:00', '2017-01-28 00:00:00', 'man.png', '-', '00007', 6, '', ''),
(18, 'BANK MANDIRI', 150000, 150000, 0, '2017-01-28 20:05:50', '2017-03-01 00:00:00', 'pic.JPG', '-', '00008', 6, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `trx_return`
--

CREATE TABLE IF NOT EXISTS `trx_return` (
`return_id` tinyint(4) NOT NULL,
  `leased_id_fk` tinyint(4) NOT NULL,
  `leased_date` date NOT NULL,
  `return_date` date NOT NULL,
  `date_of_return` date NOT NULL,
  `return_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_return_detail`
--

CREATE TABLE IF NOT EXISTS `trx_return_detail` (
  `return_detail_id` tinyint(4) NOT NULL,
  `return_id_fk` tinyint(4) NOT NULL,
  `instrument_name_fk` varchar(20) NOT NULL,
  `instrument_name` varchar(30) NOT NULL,
  `return_quantity` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ref_category`
--
ALTER TABLE `ref_category`
 ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `ref_instrument`
--
ALTER TABLE `ref_instrument`
 ADD PRIMARY KEY (`instrument_id`);

--
-- Indexes for table `ref_level`
--
ALTER TABLE `ref_level`
 ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `ref_menu`
--
ALTER TABLE `ref_menu`
 ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `ref_operator`
--
ALTER TABLE `ref_operator`
 ADD PRIMARY KEY (`operator_id`);

--
-- Indexes for table `tbl_member`
--
ALTER TABLE `tbl_member`
 ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `tbl_saldo`
--
ALTER TABLE `tbl_saldo`
 ADD PRIMARY KEY (`saldo_id`);

--
-- Indexes for table `trx_lease`
--
ALTER TABLE `trx_lease`
 ADD PRIMARY KEY (`lease_id`);

--
-- Indexes for table `trx_lease_detail`
--
ALTER TABLE `trx_lease_detail`
 ADD PRIMARY KEY (`lease_detail_id`);

--
-- Indexes for table `trx_loan_application`
--
ALTER TABLE `trx_loan_application`
 ADD PRIMARY KEY (`loan_app_id`);

--
-- Indexes for table `trx_loan_application_detail`
--
ALTER TABLE `trx_loan_application_detail`
 ADD PRIMARY KEY (`loan_app_detail_id`);

--
-- Indexes for table `trx_loan_cart`
--
ALTER TABLE `trx_loan_cart`
 ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `trx_payment`
--
ALTER TABLE `trx_payment`
 ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `trx_payment_confirm`
--
ALTER TABLE `trx_payment_confirm`
 ADD PRIMARY KEY (`payment_confirm_id`);

--
-- Indexes for table `trx_payment_temp`
--
ALTER TABLE `trx_payment_temp`
 ADD PRIMARY KEY (`payment_temp_id`);

--
-- Indexes for table `trx_return`
--
ALTER TABLE `trx_return`
 ADD PRIMARY KEY (`return_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ref_category`
--
ALTER TABLE `ref_category`
MODIFY `category_id` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ref_instrument`
--
ALTER TABLE `ref_instrument`
MODIFY `instrument_id` tinyint(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `ref_level`
--
ALTER TABLE `ref_level`
MODIFY `level_id` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ref_menu`
--
ALTER TABLE `ref_menu`
MODIFY `menu_id` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `ref_operator`
--
ALTER TABLE `ref_operator`
MODIFY `operator_id` tinyint(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_member`
--
ALTER TABLE `tbl_member`
MODIFY `member_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_saldo`
--
ALTER TABLE `tbl_saldo`
MODIFY `saldo_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `trx_lease`
--
ALTER TABLE `trx_lease`
MODIFY `lease_id` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trx_lease_detail`
--
ALTER TABLE `trx_lease_detail`
MODIFY `lease_detail_id` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trx_loan_application_detail`
--
ALTER TABLE `trx_loan_application_detail`
MODIFY `loan_app_detail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `trx_loan_cart`
--
ALTER TABLE `trx_loan_cart`
MODIFY `cart_id` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trx_payment`
--
ALTER TABLE `trx_payment`
MODIFY `payment_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trx_payment_confirm`
--
ALTER TABLE `trx_payment_confirm`
MODIFY `payment_confirm_id` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trx_payment_temp`
--
ALTER TABLE `trx_payment_temp`
MODIFY `payment_temp_id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `trx_return`
--
ALTER TABLE `trx_return`
MODIFY `return_id` tinyint(4) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
