<section id="contact" class="gray-section contact" style="padding-top:0px; ">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Hubungi Kami</h1>
                <p>Informasi Lab. & informasi kontak kami</p>
            </div>
        </div>
        <div class="row m-b-lg">
            <div class="col-lg-3 col-lg-offset-3">
                <address>
                    <strong><span class="navy">Lab. Bioantropologi & Paleoantropologi.Fakultas Kedokteran Universitas Gadjah Mada</span></strong><br/>
                    Jl. Medika,  Sekip, Sleman, Yogyakarta 55281, Indonesia
                    lab-biopaleo.fk@ugm.ac.id
                    Telp: +62-0274-552577
                    Fax: +62-0274-552577    
                </address>

            </div>
            <div class="col-lg-4">
                <p class="text-color" style="background-color: green;">
                    <img src="menejemen/admin/assets/img/logo-ugm.png" class="img-responsive">
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><strong>&copy; <?php echo date('Y'); ?> BIO-PALEONTROPOLOGI UGM</strong><br/> Sistem Informasi Penyewaan Alat-Alat BIO-PALEONTROPOLOGO UGM Berbasis Website.</p>
            </div>
        </div>
    </div>
</section>